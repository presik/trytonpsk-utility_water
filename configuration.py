# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import If, Eval, Id
from trytond.transaction import Transaction


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Utility Water Configuration'
    __name__ = 'utility_water.configuration'
    utility_water_sequence = fields.Many2One('ir.sequence', 'Utility Water Reference Sequence',
          domain=[
             ('company', 'in', [Eval('context', {}).get('company', 0), None]),
             ('sequence_type', '=',
                 Id('utility_water',
                     'sequence_type_utility_water')),
         ], required=True)
    cubic_meter_value = fields.Numeric('Cubic Meter Value')
    minimal_amount = fields.Numeric('Minimal Amount')
    subsidy = fields.Numeric('Subsidy')
    product = fields.Many2One('product.product', 'Product', domain=[('template.type', '=', 'service')])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

# utility_water.utility_reading
