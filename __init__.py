# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import utility_reading
from . import period
from . import configuration
from . import contract
from . import invoice
from . import voucher
from . import move


def register():
    Pool.register(
        utility_reading.UtilityReading,
        contract.SaleContract,
        contract.SaleContractLine,
        contract.ContractProductLine,
        invoice.Invoice,
        utility_reading.ServiceDetaill,
        period.Period,
        configuration.Configuration,
        # voucher.Voucher,
        voucher.VoucherLine,
        move.MoveLine,
        move.Move,
        module='utility_water', type_='model')
    Pool.register(
        # utility_reading.InvoiceWaterReport,
        module='utility_water', type_='report')
    # Pool.register(
    #     # period.OpenPeriod,
    #     invoice.InvoiceForceDraft,
    #     invoice.AdvancePayment,
    #     module='utility_water', type_='wizard')
