# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from datetime import timedelta, date, datetime
from dateutil.relativedelta import *

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, If, In, Get, Bool, Or
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateReport, StateTransition
from trytond.report import Report


__all__ = ['ContractProductLine', 'SaleContract']


class ContractProductLine(metaclass=PoolMeta):
    __name__ = 'sale.contract.product_line'
    utility_reading = fields.Boolean('Value From Utility Reading')

    @classmethod
    def __setup__(cls):
        super(ContractProductLine, cls).__setup__()
        cls.unit_price.states['required'] = ~Bool(Eval('utility_reading'))
        cls.unit_price.states['invisible'] = Bool(Eval('utility_reading'))


class SaleContract(metaclass=PoolMeta):
    __name__ = 'sale.contract'

    collection_amount = fields.Function(fields.Numeric('Collection Amount', readonly=True), 'get_collection_amount')
    # collection_amount = fields.Numeric('Collection Amount', readonly=False)

    def get_collection_amount(self, name):
        balance = 0
        for p in self.product_lines:
            if p.analytic_account:
                balance += p.analytic_account.balance
        balance = balance * -1 if balance != 0 else 0
        return balance

    def validate_contract(self):
        super(SaleContract, self).validate_contract()
        for line in self.product_lines:
            if line.utility_reading:
                readings = self._get_readings()
                return True if readings else False
            else:
                return True

    def _get_readings(self):
        Reading = Pool().get('utility_water.utility_reading')
        readings = Reading.search([
                ('state', '=', 'confirmed'),
                ('service.sale_contract', '=', self.id),
                ('invoice', '=', None)
            ])
        return readings
    # @classmethod
    # def update_collection_with_invoice(cls, invoice, amount):
    #     sale = invoice.sales[0] if invoice.sales else None
    #     if sale and sale.contract_line:
    #         contract = sale.contract_line.contract
    #         collection_amount = contract.collection_amount or 0
    #         cls.write([contract], {
    #             'collection_amount': collection_amount - amount
    #             })

    @classmethod
    def __setup__(cls):
        super(SaleContract, cls).__setup__()

    # @classmethod
    # def import_data(cls, fields_names, data):
    #     pool = Pool()
    #     # Product = pool.get('product.product')
    #     # Party = pool.get('party.party')
    #     # Service = pool.get('utility_water.service_detaill')
    #     Analytic = pool.get('analytic_account.account')
    #     Voucher = pool.get('account.voucher')
    #     MoveLine = pool.get('account.move.line')
    #     cont = 0
    #
    #     # dom = ('id', '<=', 2740)
    #     # contracts = cls.search([])
    #
    #     # for ct in contracts:
    #     #     print(ct.number)
    #     #     account, = Analytic.create([{
    #     #         'code': ct.reference,
    #     #         'name': ct.party.name + ' [ ' + ct.party.id_number + ' ]',
    #     #         'state': 'opened',
    #     #         'type': 'normal',
    #     #         'display_balance': 'debit-credit',
    #     #         'company': ct.company.id,
    #     #         'active': True,
    #     #         'parent': 1,
    #     #         'root': 1
    #     #     }])
    #     #     for pd in ct.product_lines:
    #     #         pd.write([pd], {'analytic_account': account.id})
    #     #
    #     #     for line in ct.lines:
    #     #         if line.sale:
    #     #             sale = line.sale
    #     #             to_create = {
    #     #                 'account': account.id,
    #     #                 'root': 1
    #     #             }
    #     #             for l in sale.lines:
    #     #                 l.write([l], {'analytic_accounts': [('create', [to_create])]})
    #     #             for inv in sale.invoices:
    #     #                 for l in inv.lines:
    #     #                     l.write([l], {'analytic_accounts': [('create', [to_create])]})
    #     #                     if inv.move:
    #     #                         for lm in inv.move.lines:
    #     #                             if lm.account.code != '130505':
    #     #                                 continue
    #     #                             to_create_ = {
    #     #                                 'account': account.id,
    #     #                                 'debit': lm.debit,
    #     #                                 'credit': lm.credit,
    #     #                                 'date': inv.move.date
    #     #                             }
    #     #                             lm.write([lm], {'analytic_lines': [('create', [to_create_])]})
    #     # print('---------------------------------------Vouchers-------------------------------')
    #     # vouchers = Voucher.search([('voucher_type', '=', 'receipt')])
    #     # for voucher in vouchers:
    #     #     for l in voucher.lines:
    #     #         if not l.sale_contract:
    #     #             continue
    #     #         if l.sale_contract.product_lines[0].analytic_account:
    #     #             to_create = {
    #     #                 'account': l.sale_contract.product_lines[0].analytic_account.id,
    #     #                 'root': 1
    #     #             }
    #     #             l.write([l], {'analytic_accounts': [('create', [to_create])]})
    #     #             if voucher.move:
    #     #                 print(voucher.move.number)
    #     #                 for lm in voucher.move.lines:
    #     #                     if lm.account.code != '130505':
    #     #                         continue
    #     #                     to_create = {
    #     #                         'account': l.sale_contract.product_lines[0].analytic_account.id,
    #     #                         'debit': lm.debit,
    #     #                         'credit': lm.credit,
    #     #                         'date': voucher.move.date
    #     #                     }
    #     #                     lm.write([lm], {'analytic_lines': [('create', [to_create])]})
    #     # print('---------------------------------------Move Lines-------------------------------')
    #     # lines = MoveLine.search([
    #     #     ('account.code', '=', '130505'),
    #     #     ('party', '!=', None),
    #     #     # ('move', '=', 71)
    #     # ])
    #     # for lm in lines:
    #     #     if not lm.analytic_lines:
    #     #         domain = [('name', '=', lm.party.name + ' [ ' + lm.party.id_number + ' ]')]
    #     #         if lm.reference:
    #     #             domain.append(
    #     #                 ('code', '=', lm.reference)
    #     #             )
    #     #         analytic = Analytic.search(domain)
    #     #         if not analytic:
    #     #             continue
    #     #         acc_id = analytic[0].id
    #     #
    #     #         to_create = {
    #     #             'account': acc_id,
    #     #             'debit': lm.debit,
    #     #             'credit': lm.credit,
    #     #             'date': lm.move.date
    #     #         }
    #     #         lm.write([lm], {'analytic_lines': [('create', [to_create])]})
    #     return cont

    def get_rec_name(self, name):
        reference = self.reference or ''
        number = self.number or ''
        return number + ' [' + reference + ']'


class SaleContractLine(metaclass=PoolMeta):
    __name__ = 'sale.contract.line'

    @classmethod
    def __setup__(cls):
        super(SaleContractLine, cls).__setup__()

    def _create_sale(self, contract):
        for line in contract.product_lines:
            if line.utility_reading:
                readings = self._get_readings()
                if not readings:
                    return None
        sale = super(SaleContractLine, self)._create_sale(contract)
        self.get_sale_line_discount(sale)
        return sale

    def get_sale_line_discount(self, sale):
        config = Pool().get('utility_water.configuration')(1)
        SaleLine = Pool().get('sale.line')
        if config.product and config.subsidy:
            product = config.product
            subsidy = config.subsidy
            total_amount = 0
            for line in sale.lines:
                total_amount += line.amount_w_tax

            if subsidy >= total_amount:
                subsidy = total_amount
            value = {
                'type': 'line',
                'sale': sale.id,
                'product': product.id,
                'quantity': 1,
                'unit': product.default_uom.id,
                'unit_price': subsidy*-1,
                'description': product.description,
            }
            line, =  SaleLine.create([value])
            return line
        else:
            return

    def get_sale_line(self, contract, line):
        config = Pool().get('utility_water.configuration')(1)

        value = super(SaleContractLine, self).get_sale_line(contract, line)
        if line.utility_reading:
            value['unit_price'] = 0
            readings = self._get_readings()
            if 'unit_price' in value.keys() and readings:
                reading = readings[0]
                value['unit_price'] = reading.consumption_amount
                if config and config.minimal_amount and (value['unit_price'] < config.minimal_amount):
                    value['unit_price'] = config.minimal_amount

        return value

    def _process_sale(self, invoice_type, period):
        pool = Pool()
        Reading = pool.get('utility_water.utility_reading')
        Date = Pool().get('ir.date')
        super(SaleContractLine, self)._process_sale(invoice_type, period)
        readings = self._get_readings()
        invoice = self.sale.invoices[0] if self.sale.invoices else None
        if invoice:
            # collection_amount = self.contract.collection_amount or 0
            # self.contract.write([self.contract], {
            #     'collection_amount': invoice.total_amount + collection_amount
            #     })
            invoice.write([invoice], {
                'invoice_date': Date.today(),
                'reference': self.contract.reference
            })
            if readings:
                invoice.write([invoice], {'utility_reading': readings[0].id})
                Reading.write([readings[0]], {
                    'invoice': invoice.id,
                    'state': 'finished'
                    })

    def _get_readings(self):
        Reading = Pool().get('utility_water.utility_reading')
        readings = Reading.search([
                ('state', '=', 'confirmed'),
                ('service.sale_contract', '=', self.contract.id),
                ('invoice', '=', None)
            ])
        return readings
