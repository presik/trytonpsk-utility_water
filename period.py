# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, Workflow, ModelSQL, fields
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateTransition
from trytond.pool import Pool
from trytond.transaction import Transaction

STATES = {'readonly': Eval('state') != 'draft'}


class Period(Workflow, ModelSQL, ModelView):
    "Period"
    __name__ = 'utility_water.period'
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    sequence = fields.Char('Sequence', readonly=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed'),
            ], 'State', readonly=True)
    start = fields.Date('Start', required=True, states=STATES, select=True,
            domain=[('start', '<=', Eval('end', None))],
            depends=['end'])
    end = fields.Date('End', required=True, states=STATES, select=True,
            domain=[('end', '>=', Eval('start', None))],
            depends=['start'])

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))
        cls._transitions |= set((
                ('draft', 'open'),
                ('open', 'draft'),
                ('open', 'closed'),
                ))
        cls._buttons.update({
                'open': {
                    'invisible': Eval('state') != 'draft',
                    },
                'close': {
                    'invisible': Eval('state') != 'open',
                    },
                'draft': {
                    'invisible': Eval('state') != 'open',
                    },
                })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(self, periods):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(self, periods):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(self, periods):
        pass

    @staticmethod
    def default_sequence():
        cursor = Transaction().connection.cursor()
        cursor.execute("SELECT MAX(CAST(coalesce(sequence, '0') AS integer)) from utility_water_period")
        res = cursor.fetchall()[0][0]
        if res:
            return str(int(res) + 1)
        else:
            return '1'

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        cursor = Transaction().connection.cursor()
        cursor.execute("SELECT MAX(sequence) from utility_water_period")
        res = cursor.fetchall()[0][0]
        if res:
            default['sequence'] = str(int(res) + 1)
        return super(Period, cls).copy(records, default=default)
