# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import date
from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.report import Report
from trytond.pyson import Get, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool
import os
# from trytond.modules.reports import InvoiceLogoReport

STATES = {
    'readonly': Eval('state') != 'draft',
}


class ServiceDetaill(Workflow, ModelSQL, ModelView):
    "Service Detaill"
    __name__ = 'utility_water.service_detaill'

    party = fields.Many2One('party.party', 'Customer', required=True, states=STATES)
    use_service = fields.Selection([
        ('', ''),
        ('residential', 'Residential'),
        ('commercial', 'Commercial'),
        ], 'Use of the Service', states=STATES)
    code_water_meter = fields.Char('Code Water Meter', states=STATES)
    sale_contract = fields.Many2One('sale.contract', 'Contract', domain=[
            ('party', '=', Eval('party')),
            ('state', '=', 'confirmed'),
        ], required=True, states=STATES)
    stratum = fields.Selection([
        ('', ''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ], 'Stratum', select=True, states=STATES)
    # customer_code = fields.Char('Customer Code', states=STATES)
    type_consumption = fields.Selection([
        ('', ''),
        ('real', 'Real'),
        ('average', 'Average'),
        ], 'Type Consumption', states=STATES)
    # diameter = fields.Numeric('Diameter', states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'State', select=True, readonly=True)
    address = fields.Char('Address', states=STATES)

    @classmethod
    def __setup__(cls):
        super(ServiceDetaill, cls).__setup__()
        # cls._order.insert(0, ('product.name', 'ASC'))
        # cls._order.insert(1, ('product.code', 'ASC'))
        cls._transitions |= set((
                ('draft', 'active'),
                ('active', 'draft'),
                ('active', 'finished'),
                ('finished', 'active'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'active',
                },
            'active': {
                'invisible': Eval('state') == 'active',
                },
            'finished': {
                'invisible': Eval('state') != 'active',
                },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('active')
    def active(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finished(cls, records):
        pass

    def get_rec_name(self, name):
        code_water = self.code_water_meter or ''
        return ' [ ' + code_water + ' ] ' + self.party.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('party.name',) + tuple(clause[1:]),
                ('party.id_number',) + tuple(clause[1:]),
                ('code_water_meter',) + tuple(clause[1:]),
                ]


class UtilityReading(Workflow, ModelSQL, ModelView):
    'Utility Reading'
    __name__ = 'utility_water.utility_reading'

    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    initial_reading = fields.Numeric('Initial Reading', required=True, states=STATES)
    final_reading = fields.Numeric('Final Reading', required=True, states=STATES)
    reading_date = fields.Date('Reading Date', required=True, states=STATES)
    consumption = fields.Function(fields.Numeric('Consumption', digits=(5, 2)),
        'get_consumption')
    period = fields.Many2One('utility_water.period', 'Period', required=True, states=STATES)
    invoice = fields.Many2One('account.invoice', 'Invoice', readonly=True)
    outstanding_balance = fields.Function(fields.Numeric('Outstanding balance', readonly=True), 'get_balance')
    service = fields.Many2One('utility_water.service_detaill', 'Service',
        required=True, states=STATES, domain=[
            ('state', '=', 'active'),
        ])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('finished', 'Finished'),
        ('cancel', 'Cancel'),
    ], 'State', readonly=True)
    party = fields.Function(fields.Many2One('party.party', 'party'), 'get_party')
    consumption_amount = fields.Function(fields.Numeric('Consumption Amount'),
                                         'get_consumption')
    previous_reading = fields.Function(fields.Many2One('utility_water.utility_reading', 'Previous Reading', readonly=True),
                                       'get_previous_reading')
    cubic_meter_value = fields.Numeric('Cubic Meter Value', required=True,  states=STATES)

    @classmethod
    def __setup__(cls):
        super(UtilityReading, cls).__setup__()
        # cls._order.insert(0, ('product.name', 'ASC'))
        table = cls.__table__()
        cls._sql_constraints += [
                   ('id_period_state_uniq', Unique(table, table.period, table.service),
                       'Reading already exists in the current period!'),
                ]
        cls._transitions |= set((
            ('draft', 'cancel'),
            ('cancel', 'draft'),
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'finished'),
            ('finished', 'confirmed'),
        ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'draft': {
                'invisible': ~Eval('state').in_(['cancel', 'confirmed']),
            },
            'confirmed': {
                'invisible': Eval('state').in_(['cancel', 'confirmed']),
            },
            'finished': {
                'invisible': Eval('state') != 'confirmed',
            },
        })

    @staticmethod
    def default_cubic_meter_value():
        config = Pool().get('utility_water.configuration')(1)
        if config:
            return config.cubic_meter_value

    @classmethod
    def set_number(cls, request):
        pool = Pool()
        config = pool.get('utility_water.configuration')(1)
        if request.number:
            return
        if not config.utility_water_sequence:
            return

        number = config.utility_water_sequence.get()
        cls.write([request], {'number': number})

    @fields.depends('initial_reading', 'period', 'service')
    def on_change_with_initial_reading(self):
        if self.period and self.service:
            readings = self.search([
                ('period.sequence', '=', str(int(self.period.sequence) - 1)),
                ('service', '=', self.service.id),
            ])
            if readings:
                return readings[0].final_reading
        return self.initial_reading

    def get_previous_reading(self, name):
        reading_id = None
        if self.period and self.service:
            before_seq = str(int(self.period.sequence) - 1)
            readings = self.search([
                ('period.sequence', '=', before_seq),
                ('service', '=', self.service.id),
            ])
            if readings:
                reading_id = readings[0].id
        return reading_id

    def get_balance(self, name):
        balance = 0
        if self.service and self.service.sale_contract:
            balance = self.service.sale_contract.collection_amount or 0
        return balance

    def get_consumption(self, name):
        if name == 'consumption':
            return round((self.final_reading - self.initial_reading), 2)
        elif name == 'consumption_amount':
            if self.consumption and self.cubic_meter_value:
                return round((self.consumption * self.cubic_meter_value),2)
            else:
                return 0

    def get_party(self, name):
        if self.customer:
            return self.customer.party.id
        else:
            return None

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, records):
        for record in records:
            if not record.number:
                cls.set_number(record)

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finished(cls, records):
        pass


# class InvoiceWaterReport(InvoiceLogoReport):
#     __name__ = 'utility_water.invoice_water'
#
#     @classmethod
#     def get_context(cls, records, header, data):
#         report_context = super().get_context(records, header, data)
#         date_ = date.today()
#         report_context['today'] = date_
#         return report_context
